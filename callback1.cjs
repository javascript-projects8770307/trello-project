const fs = require('fs');

function boardInformation(boardId, callback) {
    setTimeout(() => {
        fs.readFile('boards.json', 'utf-8', (err, data) => {
            if (err) {
                console.log(err);
            } else {
                let boardsInformation = (JSON.parse(data));
                let information = boardsInformation.filter(board => {
                    if (board.id === boardId) {
                        return board;
                    }
                })
                callback(err, information);
            }
        })
    }, Math.random()*5 * 1000);
}
module.exports = boardInformation;