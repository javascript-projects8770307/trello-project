const fs = require('fs');

function listOfBoard(boardId, callback) {
    setTimeout(() => {
        fs.readFile('lists.json', 'utf-8', (err, data) => {
            if (err) {
                console.log(err);
            } else {
                let lists = JSON.parse(data);
                callback(err, lists[boardId]);
            }
        })
    }, Math.random() * 5 * 1000);
}

module.exports = listOfBoard;