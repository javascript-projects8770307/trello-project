const fs = require('fs');
const listOfBoard = require('./callback2.cjs');
const listOfCards = require('./callback3.cjs');
const boardInformation = require('./callback1.cjs');


function ThanosBoardAllCards() {
    setTimeout(() => {
        fs.readFile('boards.json', 'utf-8', (err, data) => {
            if (err) {
                console.log(err);
            } else {
                boards = JSON.parse(data);
                let thanosData = boards.filter(board => {
                    if (board.name === "Thanos") {
                        return board;
                    }
                })
                boardInformation(thanosData[0].id, (err, data) => {
                    if (err) {
                        console.log(err);
                    } else {
                        thanosId = data[0].id;
                        console.log(data);
                        listOfBoard(thanosId, (err, data) => {
                            if (err) {
                                console.log(err);
                            } else {
                                console.log(data);
                                let cards = data.filter(list => {
                                    return list;
                                })
                                cards.forEach(card => {
                                    listOfCards(card.id, (err, data) => {
                                        if (err) {
                                            console.log(err);
                                            return;
                                        } else {
                                            console.log(card.id, data);
                                        }
                                    })
                                })
                            }
                        })
                    }
                })
            }
        })
    }, 2 * 1000)
}
module.exports = ThanosBoardAllCards;