const fs = require('fs');

function listOfCards(listId, callback) {
    setTimeout(() => {
        fs.readFile('cards.json', 'utf-8', (err, data) => {
            if (err) {
                console.log(err);
            } else {
                let cards = JSON.parse(data);
                callback(err, cards[listId]);
            }
        })
    }, Math.random() * 5 * 1000);
}

module.exports = listOfCards;